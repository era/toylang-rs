use crate::pest::Parser;
use pest::iterators::Pair;

extern crate pest;

#[derive(Parser)]
#[grammar = "../grammar/base.pest"]
pub struct ToyParser;

#[derive(Debug, PartialEq)]
pub enum Number {
    Float(f64),
    Int(i64),
}

#[derive(Debug, PartialEq)]
pub struct Ast {
    e1: Option<Box<Ast>>,
    e2: Option<Box<Ast>>,
    val: Option<Val>,
}

#[derive(Debug, PartialEq)]
pub enum Val {
    Number(Number),
    Binop(String),
}

pub fn parse(source: &str) -> Option<Box<Ast>> {
    let mut ast = None;
    let expression = ToyParser::parse(Rule::EXP, source).unwrap();
    for pair in expression {
        ast = code_gen(pair, ast);
    }
    ast
}

fn code_gen(pair: Pair<Rule>, mut ast: Option<Box<Ast>>) -> Option<Box<Ast>> {
    match pair.as_rule() {
        Rule::NUMERAL => Some(Box::new(Ast {
            e1: ast,
            e2: None,
            val: Some(Val::Number(visit_NUMERAL(pair))),
        })),
        Rule::EXPON | Rule::TERM | Rule::EXP | Rule::PRIMARY => {
            for pair in pair.into_inner() {
                ast = code_gen(pair, ast);
            }
            ast
        }
        _ => panic!("not implemented {:?}", pair.as_rule()),
    }
}

fn visit_NUMERAL(pair: Pair<Rule>) -> Number {
    for pair in pair.into_inner() {
        //TODO make it more clean
        return match pair.as_rule() {
            Rule::INT => Number::Int(pair.as_str().trim().parse::<i64>().unwrap()),
            Rule::FLOAT => Number::Float(pair.as_str().trim().parse::<f64>().unwrap()),
            _ => panic!("expecting int or float got {:?}", pair),
        };
    }
    panic!("should have at most one item in the Pair");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_number() {
        let ast = parse("5");
        assert_eq!(
            Some(Box::new(Ast {
                e1: None,
                e2: None,
                val: Some(Val::Number(Number::Int(5)))
            })),
            ast
        );
    }
}

use pest::iterators::Pair;
use wasm_bindgen::prelude::*;

use crate::pest::Parser;

extern crate pest;
#[macro_use]
extern crate pest_derive;

#[derive(Parser)]
#[grammar = "../grammar/base.pest"]
pub struct ToyParser;

#[derive(Debug, PartialEq)]
pub enum Number {
    Float(f64),
    Int(i64),
}

#[wasm_bindgen]
pub fn run(source: &str) -> i64 {
    let expression = ToyParser::parse(Rule::EXP, source).unwrap();
    for pair in expression {
        let result: Number = match pair.as_rule() {
            Rule::EXP => visit_math_op(pair),
            _ => panic!("not implemented {:?}", pair.as_rule()),
        };

        println!("{:?}", result);

        return match result {
            Number::Float(a) => a as i64, //TODO naive translation for now for wasm
            Number::Int(a) => a,
        };
    }
    return 0;
}

fn math_op_int(a: i64, b: i64, op: &str) -> Number {
    let result = match op {
        "^" => i64::pow(a, b as u32), // TODO this is bad, a lot of edge cases here
        "*" => a * b,
        "/" => a / b,
        "%" => a % b,
        "+" => a + b,
        "-" => a - b,
        _ => panic!("unknown math op: {:?}", op),
    };
    Number::Int(result)
}

fn math_op_float(a: f64, b: f64, op: &str) -> Number {
    let result = match op {
        "^" => f64::powf(a, b), // TODO this is bad, a lot of edge cases here
        "*" => a * b,
        "/" => a / b,
        "%" => a % b,
        "+" => a + b,
        "-" => a - b,
        _ => panic!("unknown math op: {:?}", op),
    };
    Number::Float(result)
}

fn math_op(a: Number, b: Number, op: &str) -> Number {
    match (a, b, op) {
        (Number::Int(a), Number::Int(b), _) => math_op_int(a, b, op),
        (Number::Float(a), Number::Int(b), "^") => Number::Float(f64::powi(a, b as i32)),
        (Number::Float(a), Number::Int(b), _) => math_op_float(a, b as f64, op),
        (Number::Float(a), Number::Float(b), _) => math_op_float(a, b, op),
        (Number::Int(a), Number::Float(b), _) => math_op_float(a as f64, b, op),
        _ => panic!("Unknown operation"),
    }
}

fn visit_NUMERAL(pair: Pair<Rule>) -> Number {
    for pair in pair.into_inner() {
        //TODO make it more clean
        return match pair.as_rule() {
            Rule::INT => Number::Int(pair.as_str().trim().parse::<i64>().unwrap()),
            Rule::FLOAT => Number::Float(pair.as_str().trim().parse::<f64>().unwrap()),
            _ => panic!("expecting int or float got {:?}", pair),
        };
    }
    panic!("should have at most one item in the Pair");
}

fn visit_math_op(pair: Pair<Rule>) -> Number {
    let mut state = vec![];
    let mut do_op = false;
    let mut op = "";

    for inner in &mut pair.into_inner() {
        match inner.as_rule() {
            Rule::NUMERAL => state.push(visit_NUMERAL(inner)),
            Rule::EXPON | Rule::TERM | Rule::EXP | Rule::PRIMARY => {
                state.push(visit_math_op(inner))
            }
            Rule::OP_A | Rule::OP_M | Rule::OP_E => {
                do_op = true;
                op = inner.as_str().trim();
                continue;
            }
            _ => panic!("invalid rule: {:?}", inner.as_rule()),
        };
        if do_op {
            do_op = false;
            let b = state.pop().unwrap();
            let a = state.pop().unwrap();
            state.push(math_op(a, b, op));
        }
    }

    if state.len() != 1 {
        panic!(
            "expecting only one number left on state vector, found {:?}",
            state
        );
    }

    state.pop().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_math() {
        println!("1 + 2 * 2 ^ 3 + (1 + 2)");
        let expression = ToyParser::parse(Rule::EXP, "1 + 2 * 2 ^ 3 + (1 + 2)").unwrap();
        for pair in expression {
            let result = match pair.as_rule() {
                Rule::EXP => visit_math_op(pair),
                _ => panic!("not implemented"),
            };
            assert_eq!(Number::Int(20), result);
        }
    }
}
